#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "PerishableProduct.h"
#include "NonperishableProduct.h"

#pragma region Phase1
std::vector<Product*> readProducts(const std::string& filename)
{
	std::ifstream productFile(filename);
	std::vector<Product*> products;

	while (!productFile.eof())
	{
		int32_t id;
		std::string name;
		float price;
		int32_t vat;
		std::string dateOrType;

		productFile >> id >> name >> price >> vat >> dateOrType;
		if (std::isdigit(dateOrType[0]))
			products.emplace_back(new PerishableProduct(id, name, price, dateOrType));
		else if (dateOrType == "Clothing")
			products.emplace_back(new NonperishableProduct(id, name, price, NonperishableProductType::Clothing));
		else if (dateOrType == "PersonalHygiene")
			products.emplace_back(new NonperishableProduct(id, name, price, NonperishableProductType::PersonalHygiene));
		else
			products.emplace_back(new NonperishableProduct(id, name, price, NonperishableProductType::SmallAppliences));
	}

	productFile.close();
	return products;
}

void printProducts(const std::vector<Product*>& products)
{
	for (const auto& p : products)
		std::cout << p->getID() << " " << p->getName() << " " << p->getPrice() << "\n";
}

void printNonPerishableProducts(const std::vector<Product*>& products)
{
	for (const auto& p : products)
	{
		if (dynamic_cast<NonperishableProduct*>(p))
			std::cout << p->getPrice() << "\n";
	}
}

bool nameCompare(Product* p1, Product* p2)
{
	return p1->getName() < p2->getName();
}

bool priceCompare(Product* p1, Product* p2)
{
	return p1->getPrice() < p2->getPrice();
}

void sortByName(std::vector<Product*>& products)
{
	std::sort(products.begin(), products.end(), nameCompare);
}


void sortByPrice(std::vector<Product*>& products)
{
	std::sort(products.begin(), products.end(), priceCompare);
}

void printSortedProducts(std::vector<Product*>& products)
{
	std::cout << "How would you like your products to be sorted?\n  1 - By Name\n  2 - By Price\n  Other - Unsorted\n";
	char option;
	std::cin >> option;
	switch (option)
	{
	case '1':
		sortByName(products);
		break;
	case '2':
		sortByPrice(products);
		break;
	default:
		break;
	}
	printProducts(products);
}
#pragma endregion Phase1

int main()
{
	std::vector<Product*> products = readProducts("Products.prodb");
	printNonPerishableProducts(products);
	printSortedProducts(products);
	return 0;
}